## 0. Overview<br />
Trong repository này, model sẽ được train để nhận diện người đeo/không đeo khẩu trang.<br />
Sau đó model được deploy để nhận diện các vật thể này thời gian thực. Hướng dẫn sẽ đi theo trình tự như sau:<br />
- Chuẩn bị dữ liệu đầu vào<br />
- Huấn luyện model<br />
- Tạo OpenCV với GPU support<br />
- Chạy model thời gian thực<br />

Các file code cung cấp đã được điều chỉnh các parameters, chỉ cần thay đổi path để chạy.

Kết quả weight & cfg sau ~700 iterations: https://drive.google.com/drive/folders/1Jgjy8ZSSqIWRFQJ_VeaghUJByOfnUIql?usp=sharing

## 1. Chuẩn bị dữ liệu đầu vào<br />
Input: Tập hình ảnh người đeo/không đeo được lấy từ Google.<br />
Ouput: File zip chứa toàn bộ ảnh và file txt chứa label, tọa độ bounding boxes.<br />

Download LabelImg: https://tzutalin.github.io/labelImg/<br />
Cách sử dụng: https://www.youtube.com/watch?v=_FNfRtXEbr4<br />

File zip output: https://drive.google.com/file/d/1zmjB8SOJubRjHPy1J9NBfb68G4EV2BmL/view?usp=sharing

Lưu ý:<br />
- Bounding boxes cần phải bao quanh cả phần đầu khi gán nhãn vào ảnh. Đã thử nghiệm bounding box bao quanh khuôn mặt với lý do để tránh những trường hợp có kiểu tóc, mũ khác biệt, tuy nhiên cách này không mang lại performance tốt, có thể bởi vì kích cỡ khuôn mặt, góc chụp của mặt làm các bounding box có kích cỡ không đồng đều.<br />
## 2. Huấn luyện model<br />
Code: https://drive.google.com/file/d/1oXit08zLDDX3ONaRr5_5FbLaN592z4hw/view?usp=sharing<br />

Model được train sử dụng GPU của Google Colab. Các cell cần lưu ý được comment #Guide ở đầu. Sau đây là giải thích một số thay đổi cho các parameter để có thể train cho số lượng class tùy chọn:<br />
#### Guide 1 - Điều chỉnh tần suất lưu weight file:<br />
- Dùng lệnh sed để thay đổi tần suất lưu file từ 1000 vòng lặp về số lượng vòng lặp theo ý muốn trong file `src/detector.c` . Điều này giúp bạn có thể sử dụng weight sớm hơn, đề phòng trường hợp vượt quá GPU usage của Google Colab, hoặc mất mạng, dẫn đến không kịp lưu weights. Để lưu lại weights sau mỗi 250 vòng lặp, sử dụng đoạn code dưới đây:<br />
`!sed -i 's/(iteration >= (iter_save + 1000) || iteration % 1000 == 0)/(iteration >= (iter_save + 250) || iteration % 250 == 0)/' src/detector.c`<br />

#### Guide 2 - Thay đổi các param của configuration file (yolo_training.cfg). Bước này gồm 2 phần:<br />
- Chuyển số lượng class tại các dòng 610, 696, 783 của file cfg theo đúng số lượng class cần train (classNum) trong trường hợp này là 2. Trong file configure, 3 dòng này thuộc 3 detection layers của Yolo dành cho 3 scales khác nhau (các layers số 82, 94, 106). Thực hiện điều chỉnh theo format như sau:<br /> 
`!sed -i '{Số dòng} s@classes=80@classes=classNum@' cfg/yolov3_training.cfg`<br />
- Chuyển số lượng class tại các dòng 603, 689, 776 theo đúng số lượng output (outputNum) của model theo công thức `(5 + Số lượng class)*Số lượng anchor`, trong trường hợp này là (5 + 2)*3 = 21. Theo format như sau:<br />
`!sed -i '{Số dòng} s@filters=255@filters=outputNum@' cfg/yolov3_training.cfg` <br />

#### Guide 3 - Tạo obj.names, obj.data<br />
- File obj.names chứa tên các đồ vật được nhận diện. Trong trường hợp này là:<br />
`!echo "pot\ncup" > data/obj.names`<br />
- File obj.data chứa thông tin về số lượng classes, đường dẫn tới file train/test, đường dẫn để lưu weights. Trong trường hợp này là:<br />
`!echo -e 'classes= 2\ntrain  = data/train.txt\nvalid  = data/test.txt\nnames = data/obj.names\nbackup = /mydrive/yolov3' > data/obj.data`<br />

Sau khi chạy cell cuối cùng, đợi cho model đạt được một mức class_loss đủ nhỏ, có thể dừng việc train. Hoặc có thể đợi cho đến khi model tự dừng train nếu mức độ loss không được cải thiện thêm. File weight được lưu tại folder yolov3 trong drive.<br />

#### Guide 4 - Train từ weight file được lưu từ lần chạy trước:<br />
- Trường hợp quá trình training gặp sự cố bị dừng lại, bạn có thể dùng file weight đã được lưu tại folder `/mydrive/yolov3` để train tiếp thay vì phải train lại từ đầu. Tiếp tục train bằng việc điều chỉnh dòng lệnh train theo format dưới đây:<br />
`!./darknet detector train data/obj.data cfg/yolov3_training.cfg {đường dẫn tới weight file} -dont_show`<br />

Note: sau khoảng 700 iterations, model detect khá tốt. Tuy nhiên nếu train lâu hơn, model sẽ label tất cả các khuôn mặt là có khẩu trang. Có thể đây là hiện tượng overfit, cần nghiên cứu thêm.<br />

## 3. Tạo GPU-enabled OpenCV<br />
Thư viện opencv cài đặt qua `pip instal opencv-python` không hỗ trợ CUDA. Vì vậy cv2.dnn sẽ không tận dụng được nguồn lực tính toán của GPU khi chạy model. Điều này gây ra hiện tượng giật lag, không đạt được mức real-time. Vì vậy cần phải tự build một bản opencv có CUDA support.<br />
Trước khi build, cần làm/cài đặt những phần mềm sau:<br />
- Tìm model GPU của máy mình tại trang https://developer.nvidia.com/cuda-gpus. Nếu có trong danh sách tức là bạn có một CUDA-enabled GPU
- Visual Studio bản 2019
- CUDA Toolkit bản 11.0
- cuDNN bản 8.0.5
- CMake bản hiện tại
- OpenCV 4.5.1
- Opencv_contrib 4.5.1
Làm theo hướng dẫn dưới đây để tạo thư viện OpenCV hỗ trợ sử dụng GPU để chạy model cài đặt trên Anaconda.<br />
Guide: https://www.youtube.com/watch?v=YsmhKar8oOc<br />
## 4. Deploy model in real-time<br />
Chạy `Train_YoloV3.ipynb` để deploy model<br />
Với bản opencv hỗ trợ CUDA, bạn có thể chạy 2 dòng lệnh dưới đây để sử dụng được GPU:<br />
`yolo.setPreferableBackend(cv2.dnn.DNN_BACKEND_CUDA)`<br />
`yolo.setPreferableTarget(cv2.dnn.DNN_TARGET_CUDA)`<br />

Về ý tưởng, ta sẽ dùng cv2.VideoCapture() để lấy từng frame từ camera. Từng frame này sẽ được đưa qua model để nhận diện, trả về bounding box & label hiển thị real-time trên cửa sổ cv2.imshow()<br />

Kết quả: (mất một chút để load) <br />
<img src="Asset/Result.gif" width="350">

## 5. Next steps<br />
- Bổ sung thêm ảnh vào tập train để tăng accuracy
- Giải quyết hiện tượng 2 bounding boxes bao quanh 1 object.
- Nghiên cứu thêm cách visualize loss, điều chỉnh early stopping như Tensorflow.
- Convert weight sang dạng file h5 để sử dụng với Tensorflow. Từ đây có thể deploy model trên Mobile app, sử dụng Tensorflow.js

## 6. Reference<br />
https://phamdinhkhanh.github.io/2020/03/09/DarknetAlgorithm.html<br />
https://towardsdatascience.com/yolo-v3-object-detection-53fb7d3bfe6b<br />
https://stackoverflow.com/questions/50390836/understanding-darknets-yolo-cfg-config-files<br />